const Discord = require('discord.js'),
    conf = require('./config'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    prefix = conf.prefix,
    token = conf.token,
    ordersFiles = fs.readdirSync('./orders').filter(f => f.endsWith('.js'));

mongoose.connect(`mongodb://${conf.user}:${conf.password}@${conf.host}:${conf.port}/${conf.db}`, {useNewUrlParser: true});

let date = new Date();
let db = mongoose.connection;
db.once('open', function () {
    console.info(`DB Connect => ${date.getDay()}/${date.getMonth()}/${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`)
});
db.on(`error`, console.error.bind(console, 'connection error:'));

const client = new Discord.Client();
client.commands = new Discord.Collection();

if (ordersFiles.length <= 0) return console.warn(`No order found`);

for (let file of ordersFiles) {
    let order = require(`./orders/${file}`);
    client.commands.set(order.name || order.alias, order);
}

client.on("message", message => {
    if (!message.content.startsWith(prefix) || message.author.bot) return;
    const args = message.content.slice(prefix.length).split(/ +/);
    const command = args.shift().toLowerCase();

    if (!client.commands.get(command)) return console.warn(`Order not found ${command}`);

    try {
        message.delete();
        client.commands.get(command).execute(message, args);
    } catch (e) {
        console.log(e);
        message.channel.send(`Un erreur c'est produit :(`)
    }

});

client.on("ready", () => {
    let date = new Date();
    console.info(`Bot Lancé => ${date.getDay()}/${date.getMonth()}/${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`)
});

client.login(token);