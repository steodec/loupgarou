const mongoose = require('mongoose');
const Party = new mongoose.Schema({
    _id: String,
    user: [{
        id: String,
        role: String
    }],
    loup: Number,
    voyante: {
        type: Boolean,
        default: false
    },
    witch: {
        type: Boolean,
        default: false
    },
    cupidon: {
        type: Boolean,
        default: false
    },
    little_girl: {
        type: Boolean,
        default: false
    },
    hunter: {
        type: Boolean,
        default: false
    }

});
module.exports = mongoose.model('party', Party);