const Discord = require('discord.js'),
    Party = require('../models/parti');
module.exports = {
    name: 'lgstop',
    alias: 'lst',
    description: 'Create a party of Loup Garoup',
    async execute(message, args) {
        Party.deleteMany({}, (err, data) => {
            if (err) {
                console.log(err);
            }
        });
        let cat = message.guild.channels.find(c => c.name === "🐺 Loup Garou" && c.type === "category");
        let channels = message.guild.channels;
        for (const channel of channels.values()) {
            if (channel.parentID === cat.id) {
                await channel.delete();
            }
        }
        cat.delete()
    },
};