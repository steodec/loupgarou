const Discord = require('discord.js'),
    Party = require('../models/parti');
module.exports = {
    name: 'lgstart',
    alias: 'lsa',
    description: 'Create a party of Loup Garoup',
    async execute(message, args) {
        const par = new Party();
        let cat = message.guild.channels.find(c => c.name === "🐺 Loup Garou");
        await Party.findOne({"loup": {$gte: 1}}).then(async p => {
            let count = p.loup + getActive(p.little_girl) + getActive(p.cupidon) + getActive(p.hunter) + getActive(p.witch) + getActive(p.voyante) + 1;
            if (p.user.length < count) {
                message.channel.send("Il n'y a pas asser de joueur il faut minimun " + (count - 1) + " joueurs");
                return
            }
            for (let i = 0; i < p.loup; i++) {
                let rand = Math.floor(Math.random() * p.user.length);
                let user = p.user[rand];
                if (user.role !== "wating") {
                    i--
                } else {
                    p.user[rand].role = 'loup';
                }
            }
            if (p.little_girl) {
                checkGirl = false;
                while (checkGirl) {
                    let rand = Math.floor(Math.random() * p.user.length);
                    let user = p.user[rand];
                    if (user.role === "wating") {
                        p.user[rand].role = "Petit Fille"
                    }
                }
            }
            if (p.cupidon) {
                checkGirl = false;
                while (checkGirl) {
                    let rand = Math.floor(Math.random() * p.user.length);
                    let user = p.user[rand];
                    if (user.role === "wating") {
                        p.user[rand].role = "Cupidon"
                    }
                }
            }
            if (p.hunter) {
                checkGirl = false;
                while (checkGirl) {
                    let rand = Math.floor(Math.random() * p.user.length);
                    let user = p.user[rand];
                    if (user.role === "wating") {
                        p.user[rand].role = "Chasseur"
                    }
                }
            }
            if (p.witch) {
                checkGirl = false;
                while (checkGirl) {
                    let rand = Math.floor(Math.random() * p.user.length);
                    let user = p.user[rand];
                    if (user.role === "wating") {
                        p.user[rand].role = "Sorciere"
                    }
                }
            }
            if (p.voyante) {
                checkGirl = false;
                while (checkGirl) {
                    let rand = Math.floor(Math.random() * p.user.length);
                    let user = p.user[rand];
                    if (user.role === "wating") {
                        p.user[rand].role = "Voyante"
                    }
                }
            }
            for (let i = 0; i<p.user.length; i++) {
                let us = message.guild.members.find(user => user.id === p.user[i].id);
                if (p.user[i].role === "wating") {
                    p.user[i].role = "Villagois"
                }
                if (p.user[i].role !== "mj") {
                    message.author.send(`${us.user.username} est un ${p.user[i].role}`);
                }
                us.send(`Ton role: **${p.user[i].role}**`);
            }
            p.save();
        });
    },
};

function getActive(bool) {
    if (bool) {
        return 1
    } else {
        return 0
    }
}