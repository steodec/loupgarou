const Discord = require('discord.js'),
    Party = require('./../models/parti');

module.exports = {
    name: 'lgcreate',
    alias: 'lc',
    description: 'Create a party of Loup Garoup',
    async execute(message, args) {
        const par = new Party();
        par.loup = 1;
        await message.guild.createChannel("🐺 Loup Garou", "category");
        let cat = message.guild.channels.find(c => c.name === "🐺 Loup Garou");
        await message.guild.createChannel("config-loupgarou", "text").then(async chan => {
            await chan.setParent(cat.id);

        });
        await startGame(message, args, par)
    },
};

async function startGame(message, args, party) {
    let conf = message.guild.channels.find(c => c.name === "config-loupgarou");
    let configue = new Discord.RichEmbed({
        title: "Loup Garou de Thiercelieux",
        description: "Configuration click sur l'emote pour config",
        color: 7,
        author: {
            name: "Loup Garou"
        },
        fields: [
            {
                name: "🐺",
                value: "Loup",
                inline: true
            },
            {
                name: "👁‍‍",
                value: "Voyante",
                inline: true
            },
            {
                name: "🌌‍‍",
                value: "Sorcière",
                inline: true
            },
            {
                name: "💘‍‍",
                value: "Cupidon",
                inline: true
            },
            {
                name: "👧‍‍",
                value: "Petite Fille",
                inline: true
            },
            {
                name: "🏹‍‍",
                value: "Chasseur",
                inline: true
            },
            {
                name: "✅‍‍",
                value: "Finir la configuration",
                inline: true
            }
        ]
    });
    conf.send(configue).then(async msg => {
        await msg.react('🐺');
        await msg.react('👁');
        await msg.react('🌌');
        await msg.react('💘');
        await msg.react('👧');
        await msg.react('🏹');
        await msg.react('✅');
        const filter = (reaction, user) => {
            return ['🐺', '👁', '🌌', '💘', '👧', '🏹', '✅'].includes(reaction.emoji.name) && user.id === message.author.id;
        };
        msg.awaitReactions(filter, {max: 1, time: 60000, errors: ['time']}).then(collected => {
            const reaction = collected.first();
            switch (reaction.emoji.name) {
                case '🐺':
                    msg.delete();
                    louconf = new Discord.RichEmbed();
                    louconf.title = 'Loup Config';
                    louconf.color = 7;
                    louconf.addField('How much', 'Choisis le nombre de loup');
                    conf.send(louconf).then(async m => {
                        await m.react(`1⃣`);
                        await m.react(`2⃣`);
                        await m.react(`3⃣`);
                        await m.react(`4⃣`);
                        const loupfilt = (reaction, user) => {
                            return ['1⃣', '2⃣', '3⃣', '4⃣'].includes(reaction.emoji.name) && user.id === message.author.id;
                        };
                        m.awaitReactions(loupfilt, {max: 1, time: 60000, errors: ['time']}).then(async collected => {
                            m.delete();
                            const reaction = collected.first();
                            switch (reaction.emoji.name) {
                                case '1⃣':
                                    party.loup = 1;
                                    break;
                                case '2⃣':
                                    party.loup = 2;
                                    break;
                                case '3⃣':
                                    party.loup = 3;
                                    break;
                                case '4⃣':
                                    party.loup = 4;
                                    break;

                            }
                            startGame(message, args, party);
                        })
                    });
                    break;
                case '👁':
                    msg.delete();
                    voyconf = new Discord.RichEmbed();
                    voyconf.title = 'Loup Config';
                    voyconf.color = 7;
                    voyconf.addField('Oui ou Non', 'Active la Voyante');
                    conf.send(voyconf).then(async m => {
                        await m.react(`✅`);
                        await m.react(`❌`);
                        const voypfilt = (reaction, user) => {
                            return ['✅', '❌'].includes(reaction.emoji.name) && user.id === message.author.id;
                        };
                        m.awaitReactions(voypfilt, {max: 1, time: 60000, errors: ['time']}).then(async collected => {
                            const reaction = collected.first();
                            if (reaction.emoji.name === "✅") {
                                m.delete();
                                party.voyante = true
                            } else {
                                party.voyante = false
                            }
                            startGame(message, args, party);
                        })
                    });
                    break;
                case '🌌':
                    msg.delete();
                    voyconf = new Discord.RichEmbed();
                    voyconf.title = 'Loup Config';
                    voyconf.color = 7;
                    voyconf.addField('Oui ou Non', 'Active la sorcière');
                    conf.send(voyconf).then(async m => {
                        await m.react(`✅`);
                        await m.react(`❌`);
                        const voypfilt = (reaction, user) => {
                            return ['✅', '❌'].includes(reaction.emoji.name) && user.id === message.author.id;
                        };
                        m.awaitReactions(voypfilt, {max: 1, time: 60000, errors: ['time']}).then(async collected => {
                            const reaction = collected.first();
                            if (reaction.emoji.name === "✅") {
                                m.delete();
                                party.witch = true
                            } else {
                                party.witch = false
                            }
                            startGame(message, args, party);
                        })
                    });
                    break;
                case '💘':
                    msg.delete();
                    voyconf = new Discord.RichEmbed();
                    voyconf.title = 'Loup Config';
                    voyconf.color = 7;
                    voyconf.addField('Oui ou Non', 'Active cupidon');
                    conf.send(voyconf).then(async m => {
                        await m.react(`✅`);
                        await m.react(`❌`);
                        const voypfilt = (reaction, user) => {
                            return ['✅', '❌'].includes(reaction.emoji.name) && user.id === message.author.id;
                        };
                        m.awaitReactions(voypfilt, {max: 1, time: 60000, errors: ['time']}).then(async collected => {
                            const reaction = collected.first();
                            if (reaction.emoji.name === "✅") {
                                m.delete();
                                party.cupidon = true
                            } else {
                                party.cupidon = false
                            }
                            startGame(message, args, party);
                        })
                    });
                    break;
                case '👧':
                    msg.delete();
                    voyconf = new Discord.RichEmbed();
                    voyconf.title = 'Loup Config';
                    voyconf.color = 7;
                    voyconf.addField('Oui ou Non', 'Active la Petit fille');
                    conf.send(voyconf).then(async m => {
                        await m.react(`✅`);
                        await m.react(`❌`);
                        const voypfilt = (reaction, user) => {
                            return ['✅', '❌'].includes(reaction.emoji.name) && user.id === message.author.id;
                        };
                        m.awaitReactions(voypfilt, {max: 1, time: 60000, errors: ['time']}).then(async collected => {
                            const reaction = collected.first();
                            if (reaction.emoji.name === "✅") {
                                m.delete();
                                party.little_girl = true
                            } else {
                                party.little_girl = false
                            }
                            startGame(message, args, party);
                        })
                    });
                    break;
                case '🏹':
                    msg.delete();
                    voyconf = new Discord.RichEmbed();
                    voyconf.title = 'Loup Config';
                    voyconf.color = 7;
                    voyconf.addField('Oui ou Non', 'Active le chasseur');
                    conf.send(voyconf).then(async m => {
                        await m.react(`✅`);
                        await m.react(`❌`);
                        const voypfilt = (reaction, user) => {
                            return ['✅', '❌'].includes(reaction.emoji.name) && user.id === message.author.id;
                        };
                        m.awaitReactions(voypfilt, {max: 1, time: 60000, errors: ['time']}).then(async collected => {
                            const reaction = collected.first();
                            if (reaction.emoji.name === "✅") {
                                m.delete();
                                party.hunter = true
                            } else {
                                party.hunter = false
                            }
                            startGame(message, args, party);
                        })
                    });
                    break;
                case '✅':
                    party.user.push({
                        id: message.author.id,
                        role: "mj"
                    });
                    party._id = message.guild.id;
                    party.save();
                    msg.delete();
                    break;
            }
        });
    })
}