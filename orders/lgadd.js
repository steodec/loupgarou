const Discord = require('discord.js'),
    Party = require('./../models/parti');

module.exports = {
    name: 'lgadd',
    alias: 'la',
    description: 'Create a party of Loup Garoup',
    async execute(message, args) {
        const par = new Party();
        let cat = message.guild.channels.find(c => c.name === "🐺 Loup Garou");
        await Party.findOne({"loup": {$gte: 1}}).then(async p => {
            await add(message, cat, p)
        });

    },
};

async function add(message, cat, party) {
    if (!message.guild.channels.find(c => c.name === '🏕丨thiercelieux' && c.parent === cat)) {
        await message.guild.createChannel('🏕丨thiercelieux', 'text').then(c => {
            c.setParent(cat);
        })
    }
    if (!message.guild.channels.find(c => c.name === '🐺丨loup')) {
        await message.guild.createChannel('🐺丨loup', 'text').then(c => {
            c.setParent(cat);
        })
    }
    if (!message.guild.channels.find(c => c.name === '👨丨rôle')) {
        await message.guild.createChannel('👨丨rôle', 'text').then(c => {
            c.setParent(cat);
        })
    }
    let user = message.mentions.users.first();
    party.user.push({
        id: user.id,
        role: "wating"
    });
    console.log(party.user);
    party.save()
}